#!/usr/bin/env python
import re
import sys
import os
from datetime import datetime
from itertools import chain
from string import Template

import ltim_reader
import ltim_validator
import fesa_reader

_CSS = """
    h1 {
     font-size: 140%;
    }

    h2 {
     font-size: 120%;
     margin-top:2em;
    }

    h3 {
     margin-top:2em;
     margin-bottom:0em
     font-weight: normal;
     font-size: 100%;
    }

    table {
      border-collapse: collapse;
    }

    th {
      padding-right:1em;
      text-align: left;
      border-bottom: 3px solid;
    }

    table tr td{
      padding: 0.1em 0.3em;
      margin:0;
      border-top: 1px solid;
      text-align: left;
    }

    ul li {
        list-style: none;
        text-indent: -2em;
        margin-bottom:0.5em;
    }

    ul.errors li { color: red; }

    ul.errors li:before {
      content: "\\274C ";
      padding-right: 0.5em;
    }

    ul.success li { color: green; }

    ul.success li:before {
      content: "\\2713 ";
      padding-right: 0.5em;
    }

    body {
      margin: 1em;
    }

    div {
      padding-left: 2em;
    }

    footer {
          margin:2em 0em;
    }

"""


def render_table(rows, filter_cols=None):
    if (filter_cols is None):
        cols = sorted(set(chain.from_iterable([row.keys() for row in rows])))
    else:
        cols = filter_cols

    html_output = "<table>"

    html_output += "<tr>"
    for field in cols:
        html_output += "<th>%s</th>" % field
    html_output += "</tr>"

    for row in rows:
        html_output += "<tr><td>"
        html_output += "\n" + "</td><td>".join([str(row.get(field, '')) for field in cols])
        html_output += "</td></tr>"

    html_output += "</table>"

    return html_output


def render_errors(errors):
    if len(errors) == 0:
        return """
            <ul class="success">
                <li>Validation OK</li>
            </ul>
        """

    return """
        <ul class="errors">
        """ + "".join(["<li>" + error + "</li>" for error in errors]) + """
        </ul>
    """


def format_omask(outputMask):
    if outputMask=='':
        outputMask=0
    omask = int(outputMask)

    if omask == 0:
        return str(omask)

    channels = []
    for i in range(1, 9):
        if (1 << i) & omask:
            channels.append("ch" + str(i))
    return str(omask) + " (" + ", ".join(channels) + ")"


def render_devices(devices):
    if len(devices) == 0:
        return ""

    for dev in devices:
        dev['name'] = Template("<a href='#${name}'>${name}</a>").substitute(name=dev['name'])
        if 'outputMask' in dev:
            dev['output'] = format_omask(dev['outputMask'])
        else:
            dev['output'] = dev['additionalOutputs']

    devices.sort(
        key=lambda ltim: int(ltim.get('lun', -1)) * 10 + int(ltim.get('channel', -1))
    )

    display_fields = ['lun', 'channel', 'name', 'loadEvent', 'mode', 'start',
                      'clock', 'delay', 'pulseWidth', 'output', 'remote']

    device_table = render_table(devices, display_fields)

    return device_table


def render_persistent_data(data):
    if len(data) == 0:
        return ""

    html = ""
    for device, device_data in data.items():
        device_data = sorted(device_data, key=lambda x: x['user'])
        html += "<a id='%s'><h3>%s</h3></a>" % (device, device)
        for row in device_data:
            if 'outputMask' in row:
                row['outputMask'] = format_omask(row['outputMask'])
        html += render_table(device_data)

    return html


def render_fec(info, devices, data, errors):
    HTML_TEMPLATE = """
    <html>
    <head>
        <title>LTIM ${fec}</title>
        <style type="text/css">""" + _CSS + """
        </style>
    </head>
    <body>
        <h1>LTIMs - ${fec}</h1>
            <div>Generated at ${timestamp}</div>
        <h2>Validation</h2>
            <div id="validation">${errors}</div>
        <h2>Info</h2>
            <div id="info">${info}</div>
        <h2>Devices</h2>
            <div id="devices">${devices}</div>
        <h2>Persistent Data</h2>
            <div id="persistence">${data}</div>
        <footer>Generated at ${timestamp}</footer>
    </body>
    </html>
    """

    html_info = render_table([info])
    html_errors = render_errors(errors)
    html_devices = render_devices(devices)
    html_data = ""
    if data is not None:
        html_data = render_persistent_data(data)

    template_vars = {
        'fec': info['fec'],
        'timestamp': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        'errors': html_errors,
        'info': html_info,
        'devices': html_devices,
        'data': html_data
    }

    return Template(HTML_TEMPLATE).substitute(template_vars)


"""
Dumps as CSV the configuration of all LTIM_DU instance files passed as arguments
"""
if __name__ == "__main__":
    instance_file = sys.argv[1]
    if len(sys.argv) == 3:
        persistence_file = sys.argv[2]
        data = ltim_reader.read_persistent_data(persistence_file)
    else:
        data = None

    # Retrieve data
    info = fesa_reader.read_info(instance_file)
    devices = ltim_reader.read_devices(instance_file)
    errors = ltim_validator.validate_ltim_config(devices, data)

    print(render_fec(info=info, devices=devices, data=data, errors=errors))
