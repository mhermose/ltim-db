#!/usr/bin/env python
from datetime import datetime
from os.path import dirname, isfile
from string import Template
#from xml.parsers.expat import ExpatError
import xml
import argparse
import os
import re
import requests
import json
import subprocess
import sys
import pickle

import fesa_reader
import ltim_reader
import ltim_validator
import ltim_to_html

DEST_DIR = 'www/' #os.path.expanduser('~')+'/cernbox/www/ltim-db/'

def getLtims(acc='*', fec='*'):

    # all these queries take a very long time, so we can cache them in a file if we're testing
    #if isfile('/tmp/ltims'):
    #    return pickle.load(open('/tmp/ltims', 'rb'))

    query='deviceClassInfo.name=="LTIM"'
    query+=' and deviceClassInfo.version=="6.1.0"'

    if acc != '*':
        query+=';accelerator="%s"'%acc

    if fec != '*':
        query+=';fecName="%s"'%fec

    print("running request %s"%query)
    # ccda has a self-signed ssl cert, ignore warning
    import urllib3
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    ltim_request = requests.get('https://ccda.cern.ch:8900/api/devices/search', params={'query':query}, verify=False)
    print("parsing request")
    print(ltim_request)
    devices = json.loads(ltim_request.content)

    all_fecs = { dev['fecName']:1 for dev in devices['content'] if dev['fecName'] }.keys()

    fecs_info = {}

    for fec in all_fecs:
        sys.stderr.write("requesing ccda info for fec %s\n"%fec)
        fec_request = requests.get('https://ccda.cern.ch:8900/api/computers/%s'%fec, verify=False)
        ccda_fec_info = json.loads(fec_request.content)

        instance = '/acc/dsc/%s/%s/data/LTIM_DU.%s.instance'%(ccda_fec_info['directory'],fec,fec)
        persistence = '/acc/dsc/%s/%s/data/LTIM_DU.%sLTIMPersistentData.xml'%(ccda_fec_info['directory'],fec,fec)

        if not isfile(instance):
            continue

        fecs_info[fec] = {'instance' : instance}

        if isfile(persistence):
            fecs_info[fec]['data'] = persistence

    # cache results to file
    #with open('/tmp/ltims', 'wb') as ltim_file:
    #    pickle.dump(fecs_info, ltim_file)
    return fecs_info

def save_html(html, filename):
    with open(filename, 'w+') as file:
        file.write(html)

def process_fecs(fecs):
    errors = {}

    # Create FEC report
    for fec,fec_info in fecs.items():
        #try:
            #import pdb; pdb.set_trace()
            sys.stderr.write("%s ...\n"%fec)
            info    = fesa_reader.read_info(fec_info['instance'])
            devices = ltim_reader.read_devices(fec_info['instance'])
            data = ltim_reader.read_persistent_data(fec_info.get('data',None))

            errors[fec] = ltim_validator.validate_ltim_config(devices, data)

            html = ltim_to_html.render_fec(info=info, devices=devices, data=data, errors=errors[fec])
            save_html(html, DEST_DIR + fec + '.html')
        #except Exception as e:
        #    sys.stderr.write("Error processing %s:\n"%fec)
        #    print(e)
        #    sys.stderr.write("\t"+str(e) + "\n")

    return errors

def create_index(fecs, errors):
    fec_list = sorted(fecs.keys())
    sorted(fec_list)

    html_index_line="<li><a href='${fec}.html'>${fec}</a></li>"

    html_index = """
    <html>
        <head>
            <title>LTIM installations</title>
            <style>"""+ltim_to_html._CSS+"""</style>
            <style>
                    ul li {
                        float:left;
                        list-style: none;
                        text-indent: 0;
                        width:15em;
                        margin-bottom:0.5em;
                        margin-right:1em;
                   }
           </style>
        </head>
    <body>
        <span>generated """+ datetime.now().strftime("%Y-%m-%d %H:%M:%S") + """</span>

        <h1>Error report</h1>
        <a href="errors.html">View Error Report</a>
        <h1>List of LTIM installations</h1>
        <ul>
    """
    for fec in fec_list:
        if fec in errors and len(errors[fec])>0:
            html_index += "<li><a href='"+fec+".html'>"+fec+" &#x274C;</a></li>"
        else:
            html_index += "<li><a href='"+fec+".html'>"+fec+"</a></li>"

    html_index += """
        </ul>
    </body>
    """
    save_html(html_index, DEST_DIR + 'index.html')

def create_validation_report(errors):

    fec_list = sorted(errors.keys())

    html_validation = """
    <html>
        <head>
            <title>LTIM validation report</title>
            <style>"""+ltim_to_html._CSS+"""</style>
        </head>
    <body>
    <span>generated """+ datetime.now().strftime("%Y-%m-%d %H:%M:%S") + """</span>
    <h1>LTIM validation report</h1>
    """

    error_found=False
    for fec in fec_list:
        if len(errors[fec])>0:
            error_found=True
            html_validation += "<b>%s</b>"%fec
            html_validation += ltim_to_html.render_errors(errors[fec])

    if not error_found:
        html_validation += "<ul class='success'><li>No LTIM validation errors found</li></ul>"

    save_html(html_validation, DEST_DIR + 'errors.html')

# Main
if __name__ == "__main__":


    parser = argparse.ArgumentParser(description='')
    parser.add_argument('destination', help='Directory where the report will be written')
    parser.add_argument('--fec', dest='fec', default='*', help='If specified, only the given front end will be updated')
    parser.add_argument('--acc', dest='acc', default='*', help='If specified, only FECs for the given accelerator will be updated')
    args = parser.parse_args()

    DEST_DIR = args.destination

    print("Retrieving fec list (this might take some minutes)\n")
    fecs = getLtims(fec=args.fec, acc=args.acc)

    print("\nFound %d FECs\n"%len(fecs))
    errors = process_fecs(fecs)
    create_index(fecs, errors)
    create_validation_report(errors)

