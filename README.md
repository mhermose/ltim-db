# LTIM-DB

Creates a website containing information about all LTIM installations containing:

- Instance file configuration
- Persistence data
- Validation of the fields. Since ccdb can't perform any validation, this can be useful to detect misconfigurations
