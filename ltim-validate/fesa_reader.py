import re
from xml.etree.ElementTree import parse


def read_info(filename):
    xml_root = parse(filename)

    info = {}

    for xml_info in list(xml_root.find('information')):
        info[xml_info.tag] = xml_info.text

    info['fec'] = re.search('_DU.(.*)', info['server-name']).group(1)
    return info


def read_devices(filename):
    xml_root = parse(filename)
    devices = []
    for xml_device in xml_root.findall('classes/*/device-instance'):
        device = {'name': xml_device.attrib['name']}

        for node in list(xml_device):
            if node.tag == 'events-mapping':
                continue
            for element in node:
                for attrib in element.attrib:
                    if attrib in ['ID', 'idref']:
                        continue
                    elif attrib == 'value':
                        # <myProperty value="xxx"> => myProperty="xxx"
                        device[element.tag] = element.attrib[attrib]
                    else:
                        # <myProperty field1="xxx"> => field1="xxx"
                        device[attrib] = element.attrib[attrib]

                if element.find('value') is not None:
                    # <myProperty><value>xxx</value></myProperty> => myProperty="xxx"
                    device[element.tag] = element.find('value').text
        devices.append(device)
    return devices


def read_persistent_data(filename):
    if filename is None:
        return {}

    xml_root = parse(filename)
    devices = {}
    for xml_device in xml_root.findall('device-instance'):
        devname = xml_device.attrib['name']
        # Extract PPM data
        ppm_properties = {}
        for node in xml_device:
            property_name = node.tag
            for ppm in list(node):
                user = ppm.attrib['name'] or "NONE"
                value = ppm.attrib['value']
                ppm_properties.setdefault(user, {})[property_name] = value

        # 'flatten' map
        devices[devname] = []
        for user, values in ppm_properties.items():
            values['user'] = user
            devices[devname].append(values)

    return devices
