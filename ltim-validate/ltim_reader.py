import fesa_reader


def format_fields(fields):
    for field, value in fields.items():
        if value is None:
            value = ''
        fields[field] = str(value)

    if 'logical-unit-number' in fields:
        fields['lun'] = fields['logical-unit-number']
        if fields['lun'] == '':
            fields['lun'] = '0'
        del fields['logical-unit-number']

    if 'omask' in fields:
        fields['outputMask'] = fields['omask']
        if fields['outputMask'] == '':
            fields['outputMask'] = 0
        del fields['omask']

    if 'pwidth' in fields:
        fields['pulseWidth'] = fields['pwidth']
        del fields['pwidth']

    return fields


def format_ltim_fields(old_fields):
    new_fields = []
    for row in old_fields:
        new_fields.append(format_fields(row))
    return new_fields


def read_devices(instance_file):
    devices = format_ltim_fields(fesa_reader.read_devices(instance_file))
    return devices


def read_persistent_data(persistence_file):
    raw_data = fesa_reader.read_persistent_data(persistence_file)

    persistence_data = {}
    for device, properties in raw_data.items():
        persistence_data[device] = format_ltim_fields(properties)
    return persistence_data
