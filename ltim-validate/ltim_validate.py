#!/usr/bin/env python
from datetime import datetime
from os.path import dirname, isfile
import xml
import argparse
import os
import requests
import json
import sys
import pickle

import ltim_reader
import ltim_validator

class bcolors:
    OK = '\033[92m' #GREEN
    FAIL = '\033[91m' #RED
    RESET = '\033[0m' #RESET COLOR

def p_success(message):
    print(f"{bcolors.OK}{message}{bcolors.RESET}")

def p_fail(message):
    print(f"{bcolors.FAIL}{message}{bcolors.RESET}")

def getLtims(acc='*', fec='*', cache_file=None):

    # all these queries take a very long time, so we can cache them in a file if we're testing
    if cache_file is not None and isfile(cache_file):
        return pickle.load(open(cache_file, 'rb'))

    query='deviceClassInfo.name=="LTIM"'
    query+=';deviceClassInfo.version=="6.1.0"'

    if acc != '*':
        query+=';accelerator=="%s"'%acc

    if fec != '*':
        query+=';fecName=="%s"'%fec

    print("running request %s"%query)
    # ignore ssl warning since ccda is self-signed
    import urllib3
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    ltim_request = requests.get('https://ccda.cern.ch:8900/api/devices/search', params={'query':query}, verify=False)
    print("parsing request")
    devices = json.loads(ltim_request.content)

    all_fecs = { dev['fecName']:1 for dev in devices['content'] if dev['fecName'] }.keys()

    fecs_info = {}

    for fec in all_fecs:
        print(f"reading {fec} info from ccda")
        fec_request = requests.get('https://ccda.cern.ch:8900/api/computers/%s'%fec, verify=False)
        ccda_fec_info = json.loads(fec_request.content)

        instance = '/acc/dsc/%s/%s/data/LTIM_DU.%s.instance'%(ccda_fec_info['directory'],fec,fec)
        persistence = '/acc/dsc/%s/%s/data/LTIM_DU.%sLTIMPersistentData.xml'%(ccda_fec_info['directory'],fec,fec)

        if not isfile(instance):
            continue

        fecs_info[fec] = {'instance' : instance}

        if isfile(persistence):
            fecs_info[fec]['data'] = persistence

    if cache_file is not None:
        with open(cache_file, 'wb') as ltim_file:
            pickle.dump(fecs_info, ltim_file)
    return fecs_info

def validate(fecs):

    validation_ok=True
    print("##########################")
    print("# LTIM VALIDATION REPORT #")
    print("##########################")
    print()
    # Create FEC report
    for fec,fec_info in fecs.items():
        devices = ltim_reader.read_devices(fec_info['instance'])

        errors = ltim_validator.validate_ltim_config(devices)

        if errors:
            p_fail(f"{fec}: ERROR")
            validation_ok=False
            for err in errors:
                p_fail(f" - {err}\n")

    if validation_ok:
        p_success("SUCCESS: no validation errors")

    print()
    print("##########################")
    print("# END OF REPORT          #")
    print("##########################")


    return validation_ok


# Main
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--fec', dest='fec', default='*', help='If specified, only the given front end will be updated')
    parser.add_argument('--acc', dest='acc', default='*', help='If specified, only FECs for the given accelerator will be updated')
    parser.add_argument('--cache', dest='cache', default=None, help='If specified, it will cache CCDE info in the given file. This will reduce execution time if the command is run a second time (useful during testing)')
    args = parser.parse_args()

    print("Retrieving fec list (this might take some minutes)\n")
    fecs = getLtims(fec=args.fec, acc=args.acc, cache_file=args.cache)

    print("\nFound %d FECs\n"%len(fecs))

    if not validate(fecs):
        sys.exit(-1)

