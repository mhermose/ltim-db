def _validate_eqpNmb(devices):
    equipment_numbers = {}
    for dev in devices:
        eqpNb = dev['eqpNb']
        if eqpNb in equipment_numbers:
            equipment_numbers[eqpNb].append(dev['name'])
        else:
            equipment_numbers[eqpNb] = [dev['name']]

    errors = []
    for eqpNb, devices in equipment_numbers.items():
        if len(devices)>1:
            errors.append('Duplicated EqpNb %s (%s)'%(eqpNb,','.join(devices)))

    return errors

def _check_num_devices(devices):
    ctr_max_num_registers = 2048
    registers = {}
    for dev in devices:
        lun = int(dev['lun'])
        registers_used = 32 if dev['mainMuxCriterion'] == 'USER' else 1
        registers_used += registers.get(lun, 0)
        registers[lun] =  registers_used

    errors=[]
    for lun, num in registers.items():
        if num > ctr_max_num_registers:
            errors.append(f"Out of memory error: lun{lun} uses {num} registers in the CTR (max: {ctr_max_num_registers})")
    return errors

def _validate_bus(devices):
    errors = []
    for dev in devices:
        if dev['publishAcquisition'] == 'true' and dev['busEnabled'] == 'false':
            errors.append(f"{dev['name']}: publishAcquisition=true but busEnabled=false")
    return errors

def _validate_outputMask(devices):
    errors = []
    for dev in devices:
        if dev['additionalOutputs'] == '{}':
            continue

        block = int((int(dev['channel'])-1)/4)
        try:
            outputs = [int(x) for x in dev['additionalOutputs'][1:-1].split(',') if x != '' ]
        except:
            errors.append(f"{dev['name']} bad format in additionalOutputs=\"{dev['additionalOutputs']}\"")
            continue
        for i in outputs:
            output_block = int((i-1)/4)
            if block != output_block:
                errors.append(f"{dev['name']} counter {dev['channel']} cant be redirected to OUT_{i}")
    return errors

def validate_ltim_config(devices):
    errors = []
    errors += _validate_eqpNmb(devices)
    errors += _check_num_devices(devices)
    errors += _validate_bus(devices)
    errors += _validate_outputMask(devices)

    return errors
